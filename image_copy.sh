#!/usr/bin/env bash

SFTP_DEV_USER=${4}
SFTP_DEV_HOST=${5}
SFTP_DEV_PATH=${6} # /opt/wwwroot/video/img
SFTP_DEV_PORT=${7}


source="${1}"
target="${2}"

ffmpeg -y -i $source -vf scale=200:-1 $target

scp -P $SFTP_DEV_PORT $target $SFTP_DEV_USER@$SFTP_DEV_HOST:$SFTP_DEV_PATH"/"${3}
