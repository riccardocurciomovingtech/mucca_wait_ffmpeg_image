#!/bin/bash
# set -xv
WAITDIR="storage"
WAITMSERVICE="storage-thumbnail"

SFTP_DEV_USER=${1}
SFTP_DEV_HOST=${2}
SFTP_DEV_PATH_IMG=${3}
SFTP_DEV_PORT=${4}

mkdir -p temp
inotifywait -m ../$WAITMSERVICE/files/$WAITDIR -e create |
    while read path action file; do
	     echo "[STORAGE FILE] waiting for files"
	     if [[ "$file" =~ .*jpg$ ]] || [[ "$file" =~ .*jpeg$ ]] || [[ "$file" =~ .*png$ ]] || [[ "$file" =~ .*PNG$ ]] || [[ "$file" =~ .*JPG$ ]] || [[ "$file" =~ .*JPEG$ ]]; then
		     echo "The file '$file' appeared in directory '$path' via '$action'"
		       ./image_copy.sh ../$WAITMSERVICE/files/$WAITDIR/$file ./temp/$file $file $SFTP_DEV_USER $SFTP_DEV_HOST $SFTP_DEV_PATH_IMG $SFTP_DEV_PORT
	     fi
    done
